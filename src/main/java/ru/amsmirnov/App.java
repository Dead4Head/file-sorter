package ru.amsmirnov;

import ru.amsmirnov.sort.MergeFileSorter;
import ru.amsmirnov.sort.Sorter;
import ru.amsmirnov.sort.generator.TextGenerator;

import java.io.File;

public class App
{
    public static void main( String[] args )
    {
//        TextGenerator generator = new TextGenerator("input.txt");
//        generator.generate();
        Sorter splitter = new MergeFileSorter(new File("input.txt"));
        splitter.sort();
    }
}
