package ru.amsmirnov.sort;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class MergeFileSorter implements Sorter {

    private final File file;
    private final int bufferSize = 100_000;

    private int numberOfTempFiles = 0;
    private int currTempDir = 0;

    public MergeFileSorter(final File fileToSort) {
        this.file = fileToSort;
    }

    @Override
    public void sort() {
        splitToSortedTemp();
        while (numberOfTempFiles > 0) {
            mergeTemp();
            numberOfTempFiles /= 2;
            currTempDir = (currTempDir + 1) % 2;
        }
        returnOutput();
        cleanTemp();
    }

    private void splitToSortedTemp() {
        List<String> lines = new ArrayList<>(bufferSize);
        try (Scanner scanner = new Scanner(new FileReader(file))) {
            while (scanner.hasNextLine()) {
                if (lines.size() == bufferSize) {
                    printToTemp(lines);
                    numberOfTempFiles++;
                    lines = new ArrayList<>(bufferSize);
                } else {
                    lines.add(scanner.nextLine());
                }
            }
            if (lines.size() > 0) {
                printToTemp(lines);
            }
        } catch (IOException e) {
            throw new SorterException(e.getMessage(), e.getCause());
        }
    }

    private void printToTemp(List<String> lines) {
        try {
            createTempDir();
            File tempFile = Paths.get("temp", Integer.toString(currTempDir), "tmp" + numberOfTempFiles + ".temp").toFile();
            tempFile.createNewFile();

            try (BufferedWriter bw = new BufferedWriter(new FileWriter(tempFile));) {
                lines = lines.stream().sorted().collect(Collectors.toList());
                for (String line : lines) {
                    bw.write(line + System.lineSeparator());
                }
            }
        } catch (IOException e) {
            throw new SorterException(e.getMessage(), e.getCause());
        }
    }

    private void createTempDir() {
        File tempDir0 = Paths.get("temp", Integer.toString(currTempDir)).toFile();
        File tempDir1 = Paths.get("temp", Integer.toString(currTempDir + 1)).toFile();
        if (!tempDir0.exists()) {
            tempDir0.mkdirs();
        }
        if (!tempDir1.exists()) {
            tempDir1.mkdirs();
        }
    }

    private void mergeTemp() {
        File file1;
        File file2;
        for (int i = 0; i < numberOfTempFiles; i = i + 2) {
            file1 = Paths.get("temp", Integer.toString(currTempDir), "tmp" + i + ".temp").toFile();
            file2 = Paths.get("temp", Integer.toString(currTempDir), "tmp" + (i + 1) + ".temp").toFile();
            try (BufferedReader reader1 = new BufferedReader(new FileReader(file1));
                 BufferedReader reader2 = new BufferedReader(new FileReader(file2));
                 BufferedWriter writer = new BufferedWriter(
                         new FileWriter(Paths.get("temp", Integer.toString((currTempDir + 1) % 2), "tmp" + i / 2 + ".temp").toFile())
                 )
            ) {
                String l1 = reader1.readLine();
                String l2 = reader2.readLine();
                while (l1 != null && l2 != null) {
                    if (l1.compareTo(l2) <= 0) {
                        writer.write(l1 + System.lineSeparator());
                        l1 = reader1.readLine();
                    } else {
                        writer.write(l2 + System.lineSeparator());
                        l2 = reader2.readLine();
                    }
                }

                while (l1 != null) {
                    writer.write(l1 + System.lineSeparator());
                    l1 = reader1.readLine();
                }
                while (l2 != null) {
                    writer.write(l2 + System.lineSeparator());
                    l2 = reader2.readLine();
                }
            } catch (IOException e) {
                throw new SorterException(e.getMessage(), e.getCause());
            }
            file1.delete();
            file2.delete();
        }
        if (numberOfTempFiles % 2 == 0) {
            file1 = Paths.get("temp", Integer.toString(currTempDir), "tmp" + numberOfTempFiles + ".temp").toFile();
            file1.renameTo(Paths.get("temp", Integer.toString((currTempDir + 1) % 2), "tmp" + (numberOfTempFiles + 1) / 2 + ".temp").toFile());
            numberOfTempFiles++;
        }
    }

    private void returnOutput() {
        Paths.get("output.txt").toFile().delete();
        File file = Paths.get("temp", Integer.toString(currTempDir), "tmp0.temp").toFile();
        file.renameTo(Paths.get("output.txt").toFile());
    }

    private void cleanTemp() {
        Paths.get("temp", "0").toFile().delete();
        Paths.get("temp", "1").toFile().delete();
        Paths.get("temp").toFile().delete();
    }

}
