package ru.amsmirnov.sort;

public class SorterException extends RuntimeException {

    public SorterException(String message, Throwable cause) {
        super(message, cause);
    }

}
