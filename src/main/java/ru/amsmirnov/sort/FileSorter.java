package ru.amsmirnov.sort;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class FileSorter implements Sorter {

    private TreeMap<String, Integer> keys = new TreeMap<>();
    private File fileToSort;

    public FileSorter(File fileToSort) {
        this.fileToSort = fileToSort;
    }

    @Override
    public void sort() {
        findKeys();
        createSortedFile();
    }

    private void findKeys() {
        try (BufferedReader br = new BufferedReader(new FileReader(fileToSort))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] splits = line.split(" : ");
                keys.merge(splits[0], 1, Integer::sum);
            }
            System.out.println(keys);
        } catch (IOException e) {
            throw new SorterException(e.getMessage(), e.getCause());
        }
    }

    private void createSortedFile() {
        try (BufferedWriter bw = new BufferedWriter((new FileWriter("output.txt")))) {
            Scanner scanner;
            for (Map.Entry<String, Integer> entry :keys.entrySet()) {
                scanner = new Scanner(fileToSort);
                for (int i = 0; i < entry.getValue(); i++) {
                    String line;
                    while (scanner.hasNextLine()) {
                        line = scanner.nextLine();
                        if (line.startsWith(entry.getKey())) {
                            bw.write(line+System.lineSeparator());
                        }
                    }
                }
                bw.flush();
            }
        } catch (IOException e) {
            throw new SorterException(e.getMessage(), e.getCause());
        }
    }

}
