package ru.amsmirnov.sort.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

public class TextGenerator {

    static String KEY_1 = "AAA_";
    static String KEY_2 = "BBB_";

    private File file;

    public TextGenerator(String filePath) {
        file = new File(filePath);
    }

    public void generate() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            int i = 0;
            while (i < 2_000_000) {
                if (i % 2 == 0)
                    bw.write(KEY_1 + i % 3 + " : " + UUID.randomUUID() + System.lineSeparator());
                else
                    bw.write(KEY_2 + i % 3 + " : " + UUID.randomUUID() + System.lineSeparator());
                i++;
            }
            bw.flush();
        } catch (IOException e) {
            throw  new RuntimeException(e.getMessage(), e.getCause());
        }
    }

}
